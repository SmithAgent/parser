<?php

require_once 'Math.php';

$math = new Math();

$answer = $math->evaluate('(2 + 3) * 4');
echo $answer.'</br>';
//var_dump($answer);

$answer = $math->evaluate('1 + 2 * ((3 + 4) * 5 + 6)');
echo $answer.'</br>';
//var_dump($answer);

$answer = $math->evaluate('(1 + 2) * (3 + 4) * (5 + 6)');
echo $answer.'</br>';
//var_dump($answer);

$math->registerVariable('a', 4);
$answer = $math->evaluate('($a + 3) * 4');
echo $answer.'</br>';
//var_dump($answer);

$math->registerVariable('a', 5);
$answer = $math->evaluate('($a + $a) * 4');
echo $answer.'</br>';
//var_dump($answer);
